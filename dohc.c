/*
 *      dohc.c - a DNS to DoH proxy.
 *      based on dohd, a DoH to DNS proxy.
 *
 *      This is free software: you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License version 2, as
 *      published by the free Software Foundation.
 *
 *
 *      dohc is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with dohc.  If not, see <http://www.gnu.org/licenses/>.
 *
 *      Author: brabo <brabo@cryptolab.net>
 *
 */
#include <wolfssl/options.h>
#include <wolfssl/ssl.h>
#include <stdint.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <wolfssl/wolfcrypt/coding.h>
#include <getopt.h>
#include <errno.h>
#include "doh.h"
#include "libevquick.h"

extern int verbose;
extern FILE *logfp;

struct client_data *clients = NULL;

static struct int_ctx *ctx = NULL;
static WOLFSSL_CTX *wctx = NULL;

static void dohc_new_connection(int __attribute__((unused)) fd,
        short __attribute__((unused)) revents, void __attribute__((unused)) *arg);

static int udp_listen(uint32_t host, uint16_t port)
{
    struct sockaddr_in serv_addr;
    int lfd = 0;

    /* Listening socket */
    lfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (lfd < 0) {
        fprintf(stderr, "ERROR: failed to create DoH socket\n");
        return -1;
    }

    /* Initialize the server address struct with zeros */
    memset(&serv_addr, 0, sizeof(serv_addr));

    /* Fill in the server address */
    serv_addr.sin_family      = AF_INET;            /* using IPv4      */
    serv_addr.sin_addr.s_addr = htonl(host);
    serv_addr.sin_port        = htons(port);    /* on DNS_PORT */


    /* Bind the server socket to our port */
    if (bind(lfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) == -1) {
        fprintf(stderr, "ERROR: failed to bind\n");
        return -1;
    }

    return lfd;
}

static int ssl_create_ctx(char *cert)
{
    /* Create and initialize WOLFSSL_CTX */
    if ((wctx = wolfSSL_CTX_new(wolfTLSv1_3_client_method())) == NULL) {
        fprintf(stderr, "ERROR: failed to create WOLFSSL_CTX\n");
        return -1;
    }

    if (wolfSSL_CTX_load_verify_locations(wctx, cert, 0)
        != SSL_SUCCESS) {
        fprintf(stderr, "ERROR: failed to load %s, please check the file.\n", cert);
        return -1;
    }

    return 0;
}

static void dohc_listen_error(int __attribute__((unused)) fd,
        short __attribute__((unused)) revents,
        void __attribute__((unused)) *arg)
{
    fprintf(stderr, "FATAL: Error on listening socket\n");
    exit(80);
}

static void dohc_client_destroy(struct client_data *cd)
{
    struct client_data *l = clients, *prev = NULL;
    if (!cd)
        return;

    /* Delete from Clients */
    while (l) {
        if (cd == l) {
            if (prev)
                prev->next = cd->next;
            else
                clients = cd->next;
            break;
        }
        prev = l;
        l = l->next;
    }
    /* Shutdown TLS session */
    if (cd->ssl) {
        if (cd->tls_handshake_done)
            wolfSSL_shutdown(cd->ssl);
        wolfSSL_free(cd->ssl);
    }
    /* Close client socket descriptor */
    close(cd->doh_sd);
    /* Remove events from file desc */
    if (cd->ev_doh)
        evquick_delevent(cd->ev_doh);
    /* free up client data */
    free(cd);
}

/**
 * Skip exactly one question record in the dns reply.
 *
 * Moves record pointer ahead, and returns the number of
 * bytes from the original position.
 */
static int dns_skip_question(uint8_t **record, int maxlen)
{
    int skip = 0;
    int incr;
    if (maxlen < (int)(*record[0]) + DNSQ_SUFFIX_LEN)
        return -1;
    while (skip < maxlen) {
        if (*record[0] == 0) {
            *record += 1 + DNSQ_SUFFIX_LEN; /* Skip fixed-size query suffix (type+class) */
            skip+= 1 + DNSQ_SUFFIX_LEN;
            return skip;
        }
        incr = 1 + *record[0];
        if (incr + skip > maxlen) {
            return -1;
        }
        *record += incr;
        skip += incr;
    }
    return skip;
}

static uint32_t dnsreply_min_age(const void *p, size_t len)
{
    int i = 0;
    const struct dns_header *hdr = p;
    uint8_t *record = ((uint8_t *)p + sizeof(struct dns_header));
    int skip = 0;
    int answers = ntohs(hdr->ancount) + ntohs(hdr->nscount) + ntohs(hdr->arcount);
    uint32_t min_ttl = 3600;
    if (answers < 1)
        return -1;

    for (i = 0; i < ntohs(hdr->qdcount); i++) {
        skip = dns_skip_question(&record, len);
        if (skip < DNSQ_SUFFIX_LEN) {
            fprintf(stderr, "Cannot parse DNS reply!\n");
            return min_ttl;
        }
        len -= skip;
    }
    for (i = 0; i < answers; i++) {
        uint32_t ttl;
        uint16_t datalen;
        if (len < 12)
            return min_ttl;
        ttl =       (record[6] << 24 ) +
                    (record[7] << 16 ) +
                    (record[8] << 8  ) +
                     record[9];
        datalen   = (record[10] << 8) +
                     record[11];
        if (len < (12U + datalen))
            return min_ttl;
        if (ttl && (ttl < min_ttl))
            min_ttl = ttl;
        record += 12 + datalen;
        len -= datalen;
    }
    return min_ttl;
}

static int client_exists(struct client_data *cd)
{
    struct client_data *c;

    if (!cd) {
        DBG("Non existent client!");
        return 0;
    }

    c = clients;
    while (c) {
        if (c == cd)
            return 1;
        c = c->next;
    }

    DBG("Non existent client!");
    return 0;
}

/**
 * send the DoH reply to DNS client
 */
static void dns_reply(struct client_data *cd, uint8_t *data, size_t len)
{
    DBG("DNS reply");
    char *hdr = (char *)data;
    char *p_clen, *start_data;
    unsigned int content_len = 0;
    int ret;

    if (!data) {
        DBG("dns_reply has no data");
        dohc_client_destroy(cd);
        return;
    }

    start_data = strstr(hdr, "\r\n\r\n");
    if (!start_data) {
        DBG("dns_reply has no start_data");
        dohc_client_destroy(cd);
        return;
    }
    start_data += 4;

    char http[800];
    memset(http, 0, 800);
    strncpy(http, hdr, (start_data - hdr));
    printf("\n\n%s%li\n\n", http, (start_data - hdr));

    if (strncmp(hdr, "HTTP/1.1 200 OK", 15)){
        DBG("dns_reply does not have HTTP/1.1 200 OK!");
        dohc_client_destroy(cd);
        return;
    }

    //if (len < DOH_RPL_MIN) {
    //    printf("dns_reply has too small a doh reply!\n");
    //    dohc_client_destroy(cd);
    //    return;
    //}
    if (!strstr(hdr, STR_CONTENT_TYPE_DNS_MSG)) {
        DBG("dns_reply has wrong content-type");
        dohc_client_destroy(cd);
        return;
    }
    p_clen = strstr(hdr, STR_CONTENT_LEN);
    if (!p_clen) {
        DBG("dns_reply has no content_len");
        dohc_client_destroy(cd);
        return;
    }
    p_clen += strlen(STR_CONTENT_LEN);

    content_len = strtol(p_clen, NULL, 10);
    if (content_len < 8) {
        DBG("dns_reply content_len is too small");
        dohc_client_destroy(cd);
        return;
    }

    if (((int)((uint8_t *)start_data - data) + (int)content_len) != ((int)len)) {
        DBG("dns_reply has incorrect len");
        dohc_client_destroy(cd);
        return;
    }

    ret = sendto(ctx->dns_sd, start_data, content_len, 0, (struct sockaddr *)&cd->dns_client, sizeof (struct sockaddr_in));
    if (ret < 0) {
        fprintf(stderr, "FATAL: localhost DNS on port 53: socket error\n");
        dohc_client_destroy(cd);
        exit(53);
    }
    dohc_client_destroy(cd);
}

/**
 * Parse the reply coming from DoH, send to DNS client
 */
static void doh_reply(__attribute__((unused)) int fd, short __attribute__((unused)) revents, void *arg)
{
    DBG("DoH reply");
    uint8_t buff[BUFSZ];
    int ret;
    struct client_data *cd;
    memset(buff, 0, BUFSZ);

    cd = (struct client_data *)arg;

    if (!client_exists(cd)) {
        DBG("client non existent!");
        return;
    }

    if (!cd->ssl) {
        DBG("ssl non existent!");
        return;
    }

    /* Read the client data into our buff array */
    ret = wolfSSL_read(cd->ssl, buff, BUFSZ);
    if (ret < 0) {
        DBG("doh read failed!");
        dohc_client_destroy(cd);
    } else {
        dns_reply(cd, buff, ret);
    }
}

/**
 * Encapsulate DNS request and send to DoH server
 */
static void doh_request(struct client_data *cd, uint8_t *data, int len)
{
    DBG("DoH request");
    char request[BUFSZ];
    int hdrlen;

    memset(request, 0, BUFSZ);

    if (!cd || !cd->ssl) {
        DBG("cd or ssl non existent!");
        return;
    }

    hdrlen = snprintf(request, BUFSZ, STR_DOH_REQUEST, ctx->dohpath, ctx->dohhost, len);
    printf("DoH request:\n%s\n\n", request);
    memcpy(request + hdrlen, data, len);

    wolfSSL_write(cd->ssl, request, hdrlen + len);
}

/**
 * Receive a DNS request from DNS client and forward to DoH server
 */
static void dns_request(struct client_data *cd)
{
    uint8_t buff[BUFSZ];
    int len;

    DBG("DNS request #%i", ctx->qcnt++);
    memset(buff, 0, BUFSZ);

    if (!client_exists(cd)) {
        DBG("client non existent!");
        return;
    }

    socklen_t slen = sizeof (struct sockaddr_in);
    len = recvfrom(ctx->dns_sd, buff, BUFSZ, 0, (struct sockaddr *)&(cd->dns_client), &slen);
    if (len < 1) {
        DBG("FATAL: DNS request on port 53: socket error: value %i\n", len);

        evquick_delevent(ctx->ev_dns);
        close(ctx->dns_sd);

        ctx->dns_sd = udp_listen(INADDR_LOOPBACK, DNS_PORT);
        if (ctx->dns_sd < 0)
            ctx->dns_sd = udp_listen(INADDR_LOOPBACK, DNS_PORT);
        if (ctx->dns_sd < 0) {
            fprintf(stderr, "Could not re-initiate udp listener!\n");
            exit(1);
        }

        ctx->ev_dns = evquick_addevent(ctx->dns_sd, EVQUICK_EV_READ, dohc_new_connection, dohc_listen_error, NULL);
        dohc_client_destroy(cd);

        return;
    }

    doh_request(cd, buff, len);
}

/**
 * Callback for error events
 */
static void net_fail(int __attribute__((unused)) fd,
        short __attribute__((unused)) revents,
        void *arg)
{
    DBG("net_fail");
    struct client_data *cd = arg;
    dohc_client_destroy(cd);
}

/**
 * Accept a new DoH connection, create client data object
 */
static void dohc_new_connection(int __attribute__((unused)) fd,
        short __attribute__((unused)) revents, void __attribute__((unused)) *arg)
{
    DBG("new DNS connection");
    struct client_data *cd = NULL;

    cd = malloc(sizeof(struct client_data));
    if (cd == NULL) {
        fprintf(stderr, "ERROR: failed to allocate memory for a new connection\n\n");
        return;
    }

    memset(cd, 0, sizeof(struct client_data));

    /* Create a WOLFSSL object */
    cd->ssl = wolfSSL_new(wctx);
    if (!cd->ssl) {
        fprintf(stderr, "ERROR: failed to create WOLFSSL object\n");
        free(cd);
        return;
    }

    /* Create local dns socket */
    cd->doh_sd = socket(AF_INET, SOCK_STREAM, 0);
    if (cd->doh_sd < 0) {
        fprintf(stderr, "ERROR: failed to create doh socket\n");
        wolfSSL_free(cd->ssl);
        free(cd);
        return;
    }

    if (connect(cd->doh_sd, (struct sockaddr *)&ctx->doh_addr, sizeof(struct sockaddr_in)) == -1) {
        fprintf(stderr, "ERROR: failed to connect to doh server\n");
        wolfSSL_free(cd->ssl);
        close(cd->doh_sd);
        free(cd);
        return;
    }

    wolfSSL_set_fd(cd->ssl, cd->doh_sd);
    wolfSSL_check_domain_name(cd->ssl, ctx->dohhost);
    cd->ev_doh = evquick_addevent(cd->doh_sd, EVQUICK_EV_READ, doh_reply, net_fail, cd);
    cd->next = clients;
    clients = cd;

    dns_request(cd);
}

static void usage(const char *name)
{

    fprintf(stderr, "%s, DNS to DNS-over-HTTPS proxy daemon.\n", name);
    fprintf(stderr, "License: GPL\n");
    fprintf(stderr, "Usage: %s -c cacert -s dohserver -p dohport [-F]\n", name);
    fprintf(stderr, "\tcacert is the ca certificate.\n");
    fprintf(stderr, "\tdohserver is the DoH server address (only IP address for now!).\n");
    fprintf(stderr, "\tdohport is the DoH server port.\n");
    fprintf(stderr, "\tUse '-F' for foreground mode\n\n");
    exit(0);
}

int main(int argc, char *argv[])
{
    char *dohsvr = NULL;
    char *cert = NULL;
    uint16_t dohport = 0;
    int option_idx;
    int c;
    int foreground = 0;
    struct option long_options[] = {
		{"help",0 , 0, 'h'},
		{"version", 0, 0, 'V'},
        {"cert", 1, 0, 'c' },
        {"dohserver", 1, 0, 's'},
        {"dohport", 1, 0, 'p'},
        {"dohhost", 1, 0, 'a'},
        {"dohpath", 1, 0, 'q'},
        {"log", 1, 0, 'l'},
        {"verbosity", 0, 0, 'v'},
        {"do-not-fork", 0, 0, 'F'},
        {NULL, 0, 0, '\0' }
    };

    logfp = stdout;
    if (!ctx)
        ctx = calloc(1, sizeof (struct int_ctx));


    while(1) {
        c = getopt_long(argc, argv, "hvc:s:p:a:q:F", long_options, &option_idx);
        if (c < 0)
            break;
        switch(c) {
            case 'h':
                usage(argv[0]);
                break;
            case 'V':
                fprintf(stderr, "%s, %s\n", argv[0], VERSION);
                exit(0);
                break;
            case 'c':
                cert = strdup(optarg);
                break;
            case 's':
                dohsvr = strdup(optarg);
                break;
            case 'p':
                dohport = (uint16_t)atoi(optarg);
                break;
            case 'a':
                ctx->dohhost = strdup(optarg);
                break;
            case 'q':
                ctx->dohpath = strdup(optarg);
                break;
            case 'v':
                verbose = 1;
                break;
            case 'l':
                logfp = fopen(optarg, "a");

                if (!logfp) {
                    perror(optarg);
                    return -1;
                }
                break;
            case 'F':
                foreground = 1;
                break;
            default:
                usage(argv[0]);
        }
    }
    setbuf(logfp, NULL);
    if (optind < argc)
        usage(argv[0]);
        /* implies exit() */

    if (!cert || !dohsvr || !dohport)
        usage(argv[0]);

    if (!foreground) {
        int pid = fork();
        if (pid > 0)
            exit(1);
        if (pid > 0)
            exit(1);
        close(STDIN_FILENO);
        close(STDOUT_FILENO);
        close(STDERR_FILENO);
        setsid();
    }

    /* Initialize wolfSSL */
    wolfSSL_Init();

    /* Initialize libevquick */
    evquick_init();

    if (!ctx->dohpath) {
        ctx->dohpath = "";
    }

    if (!ctx->dohhost) {
        ctx->dohhost = "";
    }

    ctx->dns_sd = udp_listen(INADDR_LOOPBACK, DNS_PORT);
    if (ctx->dns_sd < 0)
        ctx->dns_sd = udp_listen(INADDR_LOOPBACK, DNS_PORT);
    if (ctx->dns_sd < 0) {
        fprintf(stderr, "Could not initiate udp listener!\n");
        exit(1);
    }

    if (ssl_create_ctx(cert)) {
        fprintf(stderr, "Could not initiate SSL context!\n");
        exit(2);
    }

    ctx->doh_addr.sin_family = AF_INET;
    ctx->doh_addr.sin_port = htons(dohport);
    inet_pton(AF_INET, dohsvr, &ctx->doh_addr.sin_addr);

    ctx->ev_dns = evquick_addevent(ctx->dns_sd, EVQUICK_EV_READ, dohc_new_connection, dohc_listen_error, NULL);
    evquick_loop();

    free(dohsvr);
    free(cert);
    free(ctx);

    return 0;
}
